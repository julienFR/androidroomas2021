package com.example.roomfromgitas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.roomfromgitas.BD.Word;
import com.example.roomfromgitas.BD.WordViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private WordViewModel wordViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ViewModelFactory viewModelFactory = new ViewModelFactory(this.getApplication());
        //wordViewModel = new ViewModelProvider(this,viewModelFactory).get(WordViewModel.class);
        wordViewModel = new ViewModelProvider(this).get(WordViewModel.class);

        wordViewModel.getNbMotsLD().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                TextView textView = findViewById(R.id.tvNbMotsLD);
                textView.setText("Nb mots LD : "+ integer);
            }
        });

        wordViewModel.getAllwords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(List<Word> words) {
                TextView textView = findViewById(R.id.tvLesMots);
                String s ="";
                for (Word w : words){
                    s = s + w.getId() +" "+w.getLemot();
                }
                textView.setText(s);
            }
        });

    }


    public void viderBD(View view){
        wordViewModel.deleteAll();
    }

    public void actualiserInfos(View view){
        TextView textView = findViewById(R.id.textViewNbMots);
        textView.setText("NB Mots: " + wordViewModel.nbMots());
    }

    public void ajouterMots(View view){
        EditText editText = findViewById(R.id.editTextMot);
        wordViewModel.insert(new Word(editText.getText().toString()));
    }


}
