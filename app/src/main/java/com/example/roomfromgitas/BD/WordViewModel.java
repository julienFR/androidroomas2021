package com.example.roomfromgitas.BD;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class WordViewModel extends AndroidViewModel {
    private WordRepository wordRepository;
    private LiveData<List<Word>> allwords;
    private LiveData<Integer> nbMotsLD;



    public WordViewModel(@NonNull Application application) {
        super(application);
        wordRepository = new WordRepository(application);
        allwords = wordRepository.getAllWords();
        nbMotsLD = wordRepository.getNbMotsLD();
    }


    public LiveData<List<Word>> getAllwords() {
        return allwords;
    }

    public LiveData<Integer> getNbMotsLD() {
        return nbMotsLD;
    }

    public void insert(Word word){
        wordRepository.insert(word);
    }


    public void deleteAll(){
        wordRepository.deleteAll();
    }

    public Integer nbMots(){
        return wordRepository.getNbMots();
    }
}
