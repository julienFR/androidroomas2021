package com.example.roomfromgitas.BD;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Word.class}, version = 1)
public abstract class WordRoomDatabse extends RoomDatabase {
    public abstract WordDao wordDao();

    private static WordRoomDatabse INSTANCE;

    static WordRoomDatabse getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (WordRoomDatabse.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            WordRoomDatabse.class, "word_database").build();
                }
            }
        }
        return INSTANCE;
    }
}
