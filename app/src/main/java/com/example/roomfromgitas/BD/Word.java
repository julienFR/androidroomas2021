package com.example.roomfromgitas.BD;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "word_table")
public class Word {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "word")
    private String lemot;

    public Word(String lemot) {
        this.lemot = lemot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return lemot;
    }

    public void setWord(String lemot) {
        this.lemot = lemot;
    }

    public String getLemot() {
        return lemot;
    }

    public void setLemot(String lemot) {
        this.lemot = lemot;
    }
}
