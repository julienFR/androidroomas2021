package com.example.roomfromgitas.BD;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class WordRepository {
    private WordDao wordDao;
    private LiveData<List<Word>> allWords;
    private LiveData<Integer> nbMots;

   public WordRepository(Application application){
       WordRoomDatabse db = WordRoomDatabse.getDatabase(application);
       wordDao = db.wordDao();
       allWords = wordDao.getAllWords();
       nbMots = wordDao.nbWordsLD();
   }

    public LiveData<List<Word>> getAllWords() {
        return allWords;
    }

    public LiveData<Integer> getNbMotsLD() {
        return nbMots;
    }

    public void insert(Word word){
       new insertAsyncTask(wordDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Word,Void,Void>{
        private WordDao wordDao;

        insertAsyncTask(WordDao w){wordDao =w;}

       @Override
        protected Void doInBackground(Word... words) {
            wordDao.insert(words[0]);
            return null;
        }
    }

    public Integer getNbMots(){
       try{
           return new getNbMotsAsyncTask(wordDao).execute().get();
       }catch (Exception e){
           Log.d("MesLogs","pb getNbMots");
       }
       return null;
    }

    private static class getNbMotsAsyncTask extends AsyncTask<Void,Void,Integer>{
        private WordDao wordDao;

        getNbMotsAsyncTask(WordDao w){wordDao = w;}

        @Override
        protected Integer doInBackground(Void... voids) {
            return wordDao.nbWords();
        }
    }

    public void deleteAll(){
        Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
            @Override
            public void run() {
                wordDao.deleteAll();
            }
        });
    }
}
